var app = angular.module('cordova', ["ngRoute"]);
//#####################################
// controlador de la pagina de inicio de la aplicacion
//#####################################
app.controller('inicioCtr', ['$scope', function($scope) {

        document.getElementById("audioCapture").addEventListener("click", audioCapture);
        document.getElementById("imageCapture").addEventListener("click", imageCapture);
        document.getElementById("videoCapture").addEventListener("click", videoCapture);

        //#################### captura la imagen
        function imageCapture() {
           var options = {
              limit: 1
           };
           navigator.device.capture.captureImage(onSuccess, onError, options);

           function onSuccess(mediaFiles) {
              var i, path, len;
              for (i = 0, len = mediaFiles.length; i < len; i += 1) {
                 path = mediaFiles[i].fullPath;
                 console.log(mediaFiles);
              }
           }

           function onError(error) {
              navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
           }
        }
        //#################### captura el audio
        function audioCapture() {
           var options = {
              limit: 1,
              duration: 10
           };
           navigator.device.capture.captureAudio(onSuccess, onError, options);
           function onSuccess(mediaFiles) {
              var i, path, len;
              for (i = 0, len = mediaFiles.length; i < len; i += 1) {
                 path = mediaFiles[i].fullPath;
                 console.log(mediaFiles);
              }
           }
           function onError(error) {
              navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
           }
        }
        //#################### captura el video
        function videoCapture() {
           var options = {
              limit: 1,
              duration: 10
           };
           navigator.device.capture.captureVideo(onSuccess, onError, options);
           function onSuccess(mediaFiles) {
              var i, path, len;
              for (i = 0, len = mediaFiles.length; i < len; i += 1) {
                 path = mediaFiles[i].fullPath;
                 console.log(mediaFiles);
              }
           }
           function onError(error) {
              navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
           }
        }
}]);

app.controller('apacheCtr', ['$scope', function($scope) {
  $scope.title = "Pagina Manejada por ngRoute ";
  $scope.valor1 = 0;
  $scope.valor2 = 0;

  $scope.LS = function(){
      if(localStorage.getItem("key") == null){
          var x = 1;
          $scope.valor1 = x;
          localStorage.setItem("key",$scope.valor1);
      }else{
          var x = $scope.valor1;
          localStorage.setItem("key", 1);
          var y = parseInt(localStorage.getItem("key"));
          $scope.valor1 = x + y;
      }
  }

  $scope.SS = function(){
    if(sessionStorage.getItem("key") == null){
        var x = 1;
        $scope.valor2 = x;
        sessionStorage.setItem("key",$scope.valor2);
    }else{
        var x = $scope.valor2;
        sessionStorage.setItem("key", 1);
        var y = parseInt(sessionStorage.getItem("key"));
        $scope.valor2 = x + y;
    }
  }

}]);
//#####################################
// controlador de la pagina donde se almacenan las fotos capturadas por la aplicacion
//#####################################
app.controller('fotosCtr', ['$scope', function($scope) {
  $scope.title = "Fotos capturadas por la app";

}]);
//#####################################
// cofiguracion de rutas de la aplicacion
//#####################################
app.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    controller : "inicioCtr",
    templateUrl: 'vistas/inicio.html'
  })
  .when("/fotos", {
    controller : "fotosCtr",
    templateUrl: 'vistas/fotos.html'
  })
  .when("/albun", {
    controller : "apacheCtr",
    templateUrl: 'vistas/albun.html'
  });
});

angular.bootstrap(document, ['cordova']);
